﻿using ISSEP_Formation_API.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace ISSEP_Formation_API.Models
{
    public class ISSEPContext: DbContext
    {
        private IConfiguration Configuration;
        public ISSEPContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<Service> Services { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("ISSEPContext"));
        }
    }
}
