using ISSEP_Formation_API.Models;
using ISSEP_Formation_API.Repositories;
using ISSEP_Formation_API.RouteContraints;
using ISSEP_Formation_API.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<EmployeServiceImpl>();
builder.Services.AddSingleton<AdminService>();

builder.Services.AddSingleton<IConfiguration>(builder.Configuration);

builder.Services.Configure<RouteOptions>(ro => {
    ro.ConstraintMap.Add("email", typeof(EmailConstraint));
});
builder.Services.AddDbContext<ISSEPContext>();

builder.Services.AddScoped<AgentRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
