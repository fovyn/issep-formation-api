﻿using ISSEP_Formation_API.Models.Entities;
using ISSEP_Formation_API.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ISSEP_Formation_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AgentController: ControllerBase
    {
        private AgentRepository AgentRepository;
        public AgentController(AgentRepository repository)
        {
            AgentRepository = repository;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            List<Agent> agents = AgentRepository.FindAll();
            return Ok(AgentRepository.FindAll());
        }
    }
}
