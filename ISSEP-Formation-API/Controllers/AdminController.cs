﻿using ISSEP_Formation_API.Models;
using ISSEP_Formation_API.Services;
using Microsoft.AspNetCore.Mvc;

namespace ISSEP_Formation_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdminController : ControllerBase
    {
        private AdminService Service { get; set; }

        public AdminController(AdminService service)
        {
            Service = service;
        }
        
        [HttpGet]
        public ActionResult<List<Employe>> listAction()
        {
            return Ok(Service.Employes);
        }
    }
}
