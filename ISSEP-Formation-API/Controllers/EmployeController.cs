﻿using ISSEP_Formation_API.Models;
using ISSEP_Formation_API.Services;
using Microsoft.AspNetCore.Mvc;

namespace ISSEP_Formation_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeController: ControllerBase
    {
        public EmployeService Service { get; private set; }

        public EmployeController(EmployeServiceImpl service)
        {
            Service = service;
        }

        [HttpGet]
        public ActionResult<List<Employe>> Get()
        {
            return Ok(Service.findAll());
        }

        [HttpGet("{id}")]
        public ActionResult<Employe> Get(int id)
        {
            Employe employe = Service.FindOneById(id);
            if (employe != null)
            {
                return Ok(employe);
            }
            return NotFound($"Employe with id({id}) cannot be found");
        }

        [HttpPost]
        public ActionResult<Employe> Post([FromBody]Employe employe)
        {
            return StatusCode(201, Service.Insert(employe));
        }
    }
}
