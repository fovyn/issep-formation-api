﻿using ISSEP_Formation_API.Models;
using ISSEP_Formation_API.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace ISSEP_Formation_API.Repositories
{
    public class AgentRepository
    {
        private ISSEPContext Db;
        public AgentRepository(ISSEPContext context)
        {
            Db = context;
        }

        public List<Agent> FindAll()
        {
            return Db.Agents.ToList();
        }

        public Agent? FindOneById(int id)
        {
            return Db.Agents.FirstOrDefault(it => it.Id == id);
        }

        public Agent? Update(int id, Agent agent)
        {
            Agent? toUpdate = Db.Agents.FirstOrDefault(it => it.Id == id);
            toUpdate.Firstname = agent.Firstname;

            Db.Agents.Add(agent);
            Db.SaveChanges();

            return toUpdate;
        }
    }
}
